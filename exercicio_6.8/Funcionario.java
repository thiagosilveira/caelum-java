/*
	Classe modelo de Funcionário
*/

class Funcionario{

	private String nome;	
	private String departamento;
	private double salario;
	private String dataEntrada;
	private String rg;

	/*
		Construtor
	*/

	public Funcionario(){



	}

	public String getNome(){

		return this.nome;

	}

	public void setNome(String n){

		this.nome = n;

	}	

	public String getDepartamento(){

		return this.departamento;

	}

	public void setDepartamento(String d){

		this.departamento = d;

	}

	public double getSalario(){

		return this.salario;

	}

	public String getDataEntrada(){

		return this.dataEntrada;

	}

	public void setDataEntrada(String data){

		this.dataEntrada = data;

	}

	public String getRg(){

		return this.rg;

	}

	public void setRg(String rg){

		this.rg = rg;

	}


	void bonifica(double aumento){

		if ( aumento > 0 ){

			this.salario += aumento;

		}

	}

	double calculaGanhoAnual(){

		return this.salario *= 12;

	}

	void mostrar(){

		System.out.println("Informação do Funcionário");
		System.out.println("Nome: "+this.nome);
		System.out.println("Departamento: "+this.departamento);
		System.out.println("Salário: "+this.salario);
		System.out.println("Data Entrada: "+this.dataEntrada);
		System.out.println("RG: "+this.rg);

	}

}