/*
	Classe para testar a classe Funcionário
*/

class TestaFuncionario{

	public static void main(String[] args){

		pularLinhas();

		Funcionario f1 = new Funcionario();
		f1.nome = "Thiago Silveira";
		f1.salario = 2000.00;
		f1.departamento = "Tecnologia da Informação";
		f1.dataEntrada = "13/05/2014";
		f1.rg = "21.043.729-9";

		f1.mostrar();
		
		pularLinhas();

		// aumentando o salário
		f1.bonifica(1000.00);

		// mostrando novamente, agora com a atualização do salário
		f1.mostrar();

		pularLinhas();

		System.out.println("Ganho anual do funcionário: "+f1.calculaGanhoAnual());

		pularLinhas();

		Funcionario f2 = new Funcionario();
		f2.nome = "João Oliveira";
		f2.salario = 1000.00;
		f2.departamento = "Design";
		f2.dataEntrada = "01/02/2012";
		f2.rg = "21.980.888-1";

		System.out.println("Mostrando um novo funcionário: ");
		f2.mostrar();

		pularLinhas();

		System.out.println("Comparando o funcionário '"+f1.nome+"' com o funcionário '"+f2.nome+"'");
		if ( f1 == f2 ){
			System.out.println("Funcionário são IGUAIS");
		}else{
			System.out.println("Funcionário são DIFERENTES");
		}
			
		pularLinhas();

		f2 = f1;

		System.out.println("Segundo funcionário apontando para o primeiro");
		if ( f1 == f2 ){
			System.out.println("Funcionário são IGUAIS");
		}else{
			System.out.println("Funcionário são DIFERENTES");
		}

		pularLinhas();

	}

	public static void pularLinhas(){

		System.out.println();
		System.out.println("-------------------------------------");
		System.out.println();

	}

}
