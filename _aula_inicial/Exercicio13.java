/*

Exercício da página 32, números 1, 2 e 3

*/

class Exercicio13{

	public static void main(String[] args){

		// (1)	

		int soma = 0;
		
		System.out.println("1) Imprimindo valores de 150 até 300");

		for ( int i = 150; i <= 300; i++ ){

			System.out.println(i);
			
		}		

		// (2)	

		int n = 1;

		while ( n <= 1000 ){
			
			soma = soma + n;	

			n++;

		}

		System.out.println("2) A soma total dos valores é: "+n);
		
		// (3)
		// setando a variável novamente para o exercício 3

		System.out.println("3) Números entre 1 e 100 múltiplos de 3: ");

		n = 1;

		while ( n <= 100 ){

			if ( n % 3 == 0 ){
				System.out.println(n);
			}

			n++;

		}

	}

}
