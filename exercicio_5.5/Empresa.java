/*
	Classe modelo de Funcionário
*/

class Empresa{

	String cnpj;	
	Funcionario[] funcionarios = new Funcionario[10];
	int posicao = 0;

	void adiciona(Funcionario func){

		if ( posicao != this.funcionarios.length ){

			this.funcionarios[posicao++] = func;

		}

	}

	boolean contem(Funcionario func){

		for ( int i=0; i < posicao; i++ ){

			if ( this.funcionarios[i] == func ){

				return true;

			}

		}

		return false;

	}

	void lista(){

		for ( Funcionario f : this.funcionarios ){

			if ( f != null ){ /* verificando se existe um objeto válido para o índice do loop */

				f.mostrar();
				System.out.println();

			}

		}

	}

}