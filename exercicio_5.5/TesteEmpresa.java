/*
	Classe para testar a classe Empresa e Funcionario
*/

class TesteEmpresa{

	public static void main(String[] args){

		Empresa emp = new Empresa();

		pularLinhas();

		Funcionario f1 = new Funcionario();
		f1.nome = "Thiago Silveira";
		f1.salario = 2000.00;
		f1.departamento = "Tecnologia da Informação";
		f1.dataEntrada = "13/05/2014";
		f1.rg = "21.043.729-9";

		Funcionario f2 = new Funcionario();
		f2.nome = "Julio Cesar";
		f2.salario = 5000.00;
		f2.departamento = "Vendedor";
		f2.dataEntrada = "13/05/2014";
		f2.rg = "24.043.432-2";

		emp.adiciona(f1);
		emp.adiciona(f2);

		System.out.println("Verificando se existe o Funcionário: "+f2.nome);

		if ( emp.contem(f2) ){

			System.out.println("O funcionário existe!");

		}else{

			System.out.println("O funcionário NÃO existe!");

		}

		pularLinhas();

		emp.lista();

		pularLinhas();

	}

	public static void pularLinhas(){

		System.out.println();
		System.out.println("-------------------------------------");
		System.out.println();

	}

}
